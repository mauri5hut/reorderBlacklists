#!/bin/bash
################################################################
#
# Update blacklist file on blog.
# The file is copied from the piHole.
#
# Date:         2021-03-15
# Version:      0.1
#
# Changelog:    -
#
################################################################

LOGFILE="/var/log/reorderBlacklists.log"
htmlPage="/usr/share/nginx/html/blog/bl-content/pages/domain-blacklist/index.txt"
blacklistFolder="/usr/share/nginx/html/blog/files/blacklists"
blacklistFile="${blacklistFolder}/blacklist.txt"

if [ -e $LOGFILE ]; then

    LOGFILESIZE=$(wc -c <"$LOGFILE")
    if [ $LOGFILESIZE -gt 5000000 ]; then
        mv $LOGFILE "$LOGFILE.$(date +%Y%m%d)"
        gzip "$LOGFILE.$(date +%Y%m%d)"
    fi
fi

echo "***********************************" >> $LOGFILE
echo `date` " - Starting..." >> $LOGFILE

if [ -e /tmp/gravityList.tar ]; then

	if [ -e $blacklistFile ]; then
		rm -f $blacklistFile
	fi

	echo "Extracting gravityList.tar..." >> $LOGFILE

	tar -xf /tmp/gravityList.tar -C $blacklistFolder

	mv $blacklistFolder/gravity.list $blacklistFile

	rm -f /tmp/gravityList.tar

	echo "Modifying index.txt..." >> $LOGFILE

	oldDate=`egrep '[[:digit:]]{1,2}\.[[:digit:]]{1,2}\.[[:digit:]]{1,4}' $htmlPage | sed 's/<\/\?[^>]\+>//g' | tr -d '[:cntrl:]'`
	newDate=`date +%d.%m.%Y | tr -d '[:cntrl:]'`

	echo "Old date: ${oldDate}" >> $LOGFILE
	echo "New date: ${newDate}" >> $LOGFILE

	sed -i "s/$oldDate/$newDate/g" $htmlPage

	oldNr=`cat $htmlPage | grep "Domains" | sed 's/<\/\?[^>]\+>//g' | cut -d: -f 2 | tr -d '[:cntrl:]'`
	newNr=`wc -l $blacklistFile | cut -f1 -d' ' | tr -d '[:cntrl:]'`

        echo "Old number: ${oldNr}" >> $LOGFILE
        echo "New number: ${newNr}" >> $LOGFILE

	sed -i "s/$oldNr/$newNr/g" $htmlPage

	oldSize=`cat $htmlPage | grep "Size" | sed 's/<\/\?[^>]\+>//g' | cut -d: -f 2 | tr -d '[:cntrl:]'`
	newSize=`ls -l $blacklistFile --block-size=M | awk '{print $5}' | tr -d '[:cntrl:]'`

        echo "Old size: ${oldSize}" >> $LOGFILE
        echo "New Size: ${newSize}" >> $LOGFILE

	sed -i "s/$oldSize/$newSize/g" $htmlPage

	echo `date` " - SUCCESS." >> $LOGFILE
else
	echo "No gravityList.tar found..." >> $LOGFILE
fi